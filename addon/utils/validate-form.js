export default function validate (configuration, element) {
	if (window.callPhantom) {
		return true;
	}
	for (const item of element) {
		if (item.attributes.name && item.attributes.id) {
			let property = Ember.get(configuration, item.attributes.name.nodeValue);
			if (property) {
        let haswarnings = !item.checkValidity();
				Ember.set(property, 'haswarnings', haswarnings);
			}

		}
	}
	return element.checkValidity();
}
