export default function droppable (element, ondrop) {
  element.ondragover = (ev)=>{
    let info=[];
    for(let i=0;i<ev.dataTransfer.items.length;i++) {
      info.push(ev.dataTransfer.items[i]);
    }
    ev.preventDefault();
    let allowed = this.configuration.allowed;
    let isvalid = info.filter(i=> allowed.filter(a => i.type.indexOf(a) > -1).length > 0).length > 0;
    ev.dataTransfer.dropEffect = isvalid ? 'link': 'none';
    return !isvalid;
  };
  element.ondrop = ondrop.bind(this);
  // element.ondrop = (ev)=>{
  //     ev.preventDefault();
  //     this.actions.uploadImage.bind(this)((ev.dataTransfer || ev.detail).files[0]).then(ev =>{
  //       let response = JSON.parse(ev.target.responseText);
  //       this.get('model').set(this.get('field'), response.url);
  //     });
  //     return false;
  // };
}
