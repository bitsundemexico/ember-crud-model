const string = "abcdefghijklmnopqrstuvwxyz"; //to upper
const numeric = '0123456789';
let punctuation = '!@#$%^&*()_+~`|}{[]\:;?><,./-=';

function setCharAt(str, index, chr) {
	if (index > str.length - 1) {
		return str;
	}
	return str.substr(0, index) + chr + str.substr(index + 1);
}
export default {
	_RDN(base) {
		return Math.ceil(base.length * Math.random() * Math.random());
	},
	_ensureSpecial(password, specials, total) {
		let specialchars = punctuation.slice(0);
		let special;
		let position
		while (specials < total) {
			special = specialchars.charAt(this._RDN(punctuation));
			position = this._RDN(password);
			if (specialchars.indexOf(password[position]) === -1) {
				punctuation = specialchars.slice(0, position) + specialchars.slice(position + 1);
				password = setCharAt(password, position, special);
				specials++;
			}
		}
    return password;
	},
	_ensureNumbers(password,numbers, total) {
		let number;
		let position;
		while (numbers < total) {
			number = numeric.charAt(this._RDN(numeric));
			position = this._RDN(password);
			if (punctuation.indexOf(password[position]) === -1 && numeric.indexOf(password[position]) === -1) {
				password = setCharAt(password, position, number);
				numbers++;
			}
		}
    return password;
	},
	gen(configuration) {
    punctuation = '!@#$%^&*()_+~`|}{[]\:;?><,./-=';
		configuration.restrict_chars.forEach(function (char) {
			let index = punctuation.indexOf(char);
			punctuation = punctuation.slice(0, index) + punctuation.slice(index + 1);
		});
		let password = "";
		let numbers = 0;
		let mayus = 0;
		let specials = 0;
		while (password.length < configuration.length) {
			let cmp = Math.ceil(Math.exp(Math.random() * configuration.length));
			let generator = string;
			if (cmp % 3 === 0 && configuration.numbers) {
				generator = numeric;
				numbers++;
			} else if (cmp % 5 === 0 && configuration.special_chars) {
				generator = punctuation;
				specials++;
			}
			let entity = Math.ceil(this._RDN(generator));
			entity = generator.charAt(entity);
			let makemayus = this._RDN(password) % 2;
			if (makemayus) {
				mayus++;
			}
			password += makemayus ? entity : entity.toUpperCase();
		}
		password = this._ensureSpecial(password, specials, configuration.specialChars);
		password = this._ensureNumbers(password, numbers, configuration.numbers);
		return password;
	}
};
