import Ember from 'ember';
export default function uploadimage (root, uploadpath, file) {
	var configuration = arguments.length > 3 && arguments[3] !== undefined ? arguments[3]: [];
	var progress = arguments.length > 4 && arguments[4] !== undefined ? arguments[4]: function () {};

	return new Promise(function (resolve, reject) {
		var xhr = new XMLHttpRequest();
		xhr.upload.addEventListener('progress', progress, false);
		xhr.addEventListener('load', function (ev) {
			var res = JSON.parse('[' + ev.target.responseText.replace(/\}\{/g, '},{') + ']');
			resolve(res[res.length - 1]);
		});
		xhr.addEventListener('error', reject);
		xhr.addEventListener('abort', reject);
		xhr.open('POST', uploadpath, true);
		var data = new FormData();
		data.append('upload-file', file, file.name);
		data.append('configuration', JSON.stringify(configuration));
		xhr.send(data);
	});
}
