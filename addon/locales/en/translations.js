export default {
  'crud-model':{
    'drop_leyend': 'Drop your image here',
    filterset:{
      'or': 'or',
      'and': 'and',
      '===': 'Is',
      '==': 'Is',
      '!=': 'Is not',
      '>': 'Bigger',
      '>=': 'Bigger or Equal',
      '<': 'Smaller',
      '<=': 'Smaller or Equal',
      'like': 'Contains'
    }
  }
};
