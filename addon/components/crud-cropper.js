import Ember from 'ember';
import layout from 'ember-crud-model/templates/components/crud-cropper';

var URL = window.URL || window.webkitURL;
function dataURItoBlob (dataURI) {
	// convert base64 to raw binary data held in a string
	// doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
	var byteString = atob(dataURI.split(',')[1]);

	// separate out the mime component
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

	// write the bytes of the string to an ArrayBuffer
	var ab = new ArrayBuffer(byteString.length);

	// create a view into the buffer
	var ia = new Uint8Array(ab);

	// set the bytes of the buffer to the correct values
	for (var i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i);
	}

	// write the ArrayBuffer to a blob, and you're done
	var blob = new Blob([ab], { type: mimeString });
	return blob;
}
export default Ember.Component.extend({
	dirty: Ember.computed('cropper', function () {
		return this.get('cropper') != null;
	}),
	width: 1280,
	height: null,
	size: Ember.computed('width', 'aspectRatio', function () {
		return {
			width: this.get('width'),
			height: this.get('width') * this.get('aspectRatio')
		};
	}),
  ratios: ['1:1'],
	ratio: '1:1',
	cropper: null,
  tagName: 'section',
  classNames: ["crud-model-cropper"],
  hide: true,
  classNameBindings: ['hide'],
  choseratios: false,
	create (file) {
		var img = this.element.querySelector('img');
		if (img.src && file) {
      this.set('orginal_src', img.src);
			URL.revokeObjectURL(img.src);
		}
		if (this.get('cropper')) {
			this.get('cropper').destroy();
			this.set('cropper', null);
		}
		if (file) {
			var filename = file.name.split('.');
			filename.splice(filename.length - 1, 1);
			filename = filename.join('.');
			this.set('filename', file.name);
      this.set('type', file.type);
			img.src = file = URL.createObjectURL(file);
      this.set('raw', file);
		}
		this.set('aspectRatio', this.settings.aspectRatio);
		this.set('cropper', new Cropper(img, this.settings));
	},
	actions: {
    rotate (deg) {
      this.get('cropper').rotate(deg);
    },
    cancel() {
        this.set('raw', null);
        this.set('hide', true);
        this.attrs.cancelCrop && this.attrs.cancelCrop.bind(this)()
        this.set('orginal_src', null);
    },
    zoom (deg) {
      this.get('cropper').zoom(deg);
    },
		updateImage: function updateImage () {
			if (this.get('cropper')) {
				this.get('cropper').destroy();
				this.set('cropper', null);
			}
			$(this.element.querySelector('[data-role=image-edit]')).modal('close');
			this.attrs.refresh();
		},
		updateRatio: function updateRatio (ratio) {
      this.set('ratio', ratio);
			var parsed = ratio.split(':');
			parsed = parsed[0] / parsed[1];
			this.set('settings.aspectRatio', parsed);
			this.set('aspectRatio', parsed);
      this.set('choseratios', false);
			this.create();
		},
		selectNewImage: function selectNewImage () {
			$('#image-edit-button').click();
		},
		cropImage: function cropImage () {
			var that = this;
			var size = {
				width: this.get('width'),
				height: this.get('width') / this.get('aspectRatio')
			};
			var blob = dataURItoBlob(that.get('cropper').getCroppedCanvas().toDataURL(this.get('type'), 0.7));
			blob.name = that.get('filename');
			that.attrs.aftercrop.bind(that)(blob).then(function (response) {
				that.get('cropper').destroy();
				that.set('cropper', null);
				$(that.element.querySelector('[data-role=image-edit]')).modal('close');
        that.get('wait').resolve();
				return response;
			});
		}
	},
	configuration: null,
	layout,
	file: null,
	close: function close () {
    this.set('hide', true);
	},
	show () {
    this.set('wait', new $.Deferred());
		var _this = this;
		var file = arguments.length > 0 && arguments[0] !== undefined ? arguments[0]: null;
		var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1]: undefined;
    this.create(file);
    this.set('size', size);
    this.set('hide', false);
		// $(this.element.querySelector('[data-role=image-edit]')).off('shown.bs.modal');
		// $(this.element.querySelector('[data-role=image-edit]')).on('shown.bs.modal', function () {
		//
		// });
		// $(this.element.querySelector('[data-role=image-edit]')).modal({ backdrop: 'static', keyboard: false });
    return this.get('wait');
	},
	init () {
		this._super.apply(this, arguments);
		this.attrs.open('cropper', this);
		var settings = Object.assign({}, { preview: '.preview', aspectRatio: ['1:1'] }, this.configuration);
		this.set('ratios', settings.aspectRatio.length > 1);
		settings.aspectRatio = settings.aspectRatio[0].split(':');
		settings.aspectRatio = settings.aspectRatio[0] / settings.aspectRatio[1];
		this.set('settings', settings);
	},
  didInsertElement() {
    $(this.element).find('.dropdown-button').dropdown({
      // inDuration: 300,
      // outDuration: 225,
      // constrainWidth: false,
      // hover: true, // Activate on hover
      // gutter: 0, // Spacing from edge
      belowOrigin: true,
      alignment: 'left',
      stopPropagation: true
    }
  );
  },
  didRender () {
    $('body').append(this.element);
  }
});
