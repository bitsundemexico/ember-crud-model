import Ember from 'ember';
import passgen from 'ember-crud-model/utils/passgen';
import droppable from 'ember-crud-model/utils/droppable';
import uploadimage from 'ember-crud-model/utils/uploadimage';

// function _toConsumableArray (arr) {
// 	if (Array.isArray(arr)) {
// 		for (let i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
// 			arr2[i] = arr[i];
// 		}
//
// 		return arr2;
// 	} else {
// 		return Array.from(arr);
// 	}
// }

let templates = [];
for (let moduleKey in require._eak_seen) {
	if (moduleKey.indexOf('\/templates\/') !== -1) {
		if (moduleKey.indexOf('crud-model/fields/') > -1) {
			templates.push(moduleKey);
		}
	}
}

export default Ember.Component.extend({
	tempvalue: null,
	selected: null,
	configuration: null,
	materialize: true,
  progress: -1,
  isUploading: false,
	responsive: null,
	size: Ember.computed('configuration.size', 'materialize', function () {
		if (this.get('materialize')) {
			if(this.get('configuration.responsive')) {
				return false;
			}
			let size = this.get('configuration.size');
			return 's' + (size || 12);
		}
		return false;
	}),
	i18n: Ember.inject.service(),
	config: Ember.inject.service('ember-crud-model-config'),
	actions: {
    cancelCrop () {
      // this.get('model').set(this.get('field'), this.get('cropper.orginal_src'));
    },
		opencropper: function opencropper () {
			this.get('cropper').show();
		},
		vinculate: function vinculate (ref, component) {
			this.set(ref, component);
		},
		select: function select (value) {
			this.set('selected', value);
		},
		preupload: function preupload () {
			$(this.element.querySelector('[data-role=ember-crud-model-upload-file]')).click();
		},
		set: function set (target, property, value) {
			Ember.set(target, property, value);
		},
		setsearch: function setsearch (state) {
			this.set('search', state);
			if (state) {
				// let model = this.get('model');
				// let initialvalue = model.get(`${this.field}.${this.configuration.source}`);
				this.set('searchterm', '');
				setTimeout(() => {
					$('input[name=' + this.field + ']').focus();
				}, 100);
				let mode = this.configuration.type === 'dropdown' || this.configuration.type === 'catalog';
				this.actions.search.bind(this)(null, null, mode ? 0 : 2000);
			}
		},
		updatevalue: function updatevalue (value) {
			this.model.set(this.field, value);
			this.set('search', false);
			this.actions.generic.bind(this)(value);
		},
		togglesearch: function togglesearch () {
			this.actions.setsearch.bind(this)(!this.get('search'));
		},
		search: function search (word, ev) {
			let time = arguments.length > 2 && arguments[2] !== undefined ? arguments[2]: 700;

			if (this.configuration.template === 'dropdown') {
				time = 0;
			}
			clearTimeout(this.timeout);
			let query = {};
			if (word) {
				if (this.configuration.mask) {
					query[this.configuration.mask] = word;
				} else {
					query[this.configuration.source] = word;
				}
			}
			this.set('searching', true);
			this.timeout = setTimeout( () => {
				let actions = this.configuration.catalog ? this.configuration.catalog.records : this.configuration.records;
				this._targetObject.sideModels[actions].bind(this._targetObject)(query, this, this._targetObject)
					// this.findrecords(query)
					.then((records) => {
						this.set('searching', false);
						Ember.set(this, 'records', records);
					});
			}, time);
		},

		newpass: function newpass (record, field) {
			record.set(field, passgen.gen(this.get('config.password-policy')));
		},
		uploadImage (file) {
			let that = this;
			that.set('isUploading', true);
			this.set('progress', -1);
			this.get('cropper').close();

			return (0, uploadimage)(this, this.configuration.upload, file, that.get('configuration.data'), (ev) => {
				let percentComplete = ev.loaded / ev.total;
				percentComplete = parseInt(percentComplete * 100);
				this.set('progress', percentComplete);
			}).then(response => {
        that.set('isUploading', false);
        that.set('progress', -1);
        if (this.attrs.afterUpload) {
          that.attrs.afterUpload.bind(that)(response);
        } else {
				    that.get('model').set(that.get('field'), response.url);
        }
			});
		},
		generic: function generic () {
			if (this.configuration.action) {
				// for (let _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
				// 	args[_key] = arguments[_key];
				// }

				if (this._targetObject.genericActions) {
					// let _targetObject$generic;
          this._targetObject.genericActions[this.configuration.action](...arguments, this, this._targetObject);

					// (_targetObject$generic = this._targetObject.genericActions)[this.configuration.action].apply(_targetObject$generic, _toConsumableArray(args).concat([this, this._targetObject]));
				} else {
					// let _targetObject$actions;
          this._targetObject.actions[this.configuration.action](...arguments, this, this._targetObject);
					// (_targetObject$actions = this._targetObject.actions)[this.configuration.action].apply(_targetObject$actions, _toConsumableArray(args).concat([this, this._targetObject]));
				}
			}
		}
	},
	mode: 'read',
	layoutName: Ember.computed('configuration', function () {
		let template = 'edit-' + (this.get('configuration.template') || this.get('configuration.type'));
		template = templates.any(function (t) {
			return t.indexOf(template) > -1;
		}) ? template : 'edit-text';
		return 'crud-model/fields/' + template;
	}),
	timeout: 0,
	tagName: 'td',
	materializeClass: Ember.computed('configuration.haswarnings', 'configuration.placeholder', 'configuration.sufix', function () {
		return (this.get('configuration.haswarnings') ? ' invalid' : '') + (this.get('configuration.placeholder') ? ' active' : '') + (this.get('configuration.sufix') ? ' sufix' : '');
	}),
	classNameBindings: ['configuration.haswarnings:has-warning:', 'ispassword:no-padding', 'configuration.sufix:row:', 'useinput:input-field', 'materialize:col:', 'size', 'configuration.responsive'],
	ispassword:Ember.computed('configuration.type', function() {
		return this.get('configuration.type') === 'password';
	}),
  useinput:Ember.computed('configuration.type', function(){
    return ['geolocation', 'switch', 'password'].indexOf(this.get('configuration.type')) === -1;
  }),
	init: function init () {
		this._super.apply(this, arguments);
		this.set('labelSize', this._targetObject.labelSize);
		this.set('inputSize', 12 - this._targetObject.labelSize);
		if (this.configuration.records) {
			this.set('search', false);
		}
		let ratios = this.configuration.cropper && this.configuration.cropper.aspectRatio && this.configuration.cropper.aspectRatio.length > 1;

		this.set('id', this.configuration.id || this.field);
		if (this.configuration.type === 'image') {
      this.set('ratios', this.configuration.cropper.ratios || ['1:1']);
		}
		if (this.configuration.catalog && !this.configuration.catalog.ignoreInitial) {
			this._targetObject.sideModels[this.configuration.catalog.records]().then((records) => {
				Ember.set(this, 'records', records);
			});
		}
    this.set('hasvalue', !!this.get('model').get(this.field));
    if (this.configuration.type === 'geolocation') {
			Ember.set(this.configuration, 'zoom', this.get('configuration.zoom') || 'zoom');
		}
	// if(this.configuration.default) {
	//   this.get('model').set(this.get('field'), this.configuration.default);
	// }
	},
	didInsertElement: function didRender () {
		let that = this;
		this._super.apply(this, arguments);
		if (this.configuration.upload) {
			let inputFile = this.element.querySelector('[data-role=ember-crud-model-upload-file]');
      function get(array, fn) {
        let item = array.shift();
        if(item) {
          fn(item).then(()=>{
            get(array, fn);
          });
        }
      }
			if (Cropper) {
				droppable.bind(this)(this.element, (ev) => {
					ev.preventDefault();
					ev.stopPropagation();
          let files = [];
          Array.prototype.push.apply( files, (ev.dataTransfer || ev.detail).files );
          get(files, (file)=>{
            return this.get('cropper').show(file);
          });
					return false;
				});
			} else {
				droppable.bind(this)(this.element, (ev) => {
					ev.preventDefault();
					this.actions.uploadImage.bind(this)((ev.dataTransfer || ev.detail).files[0]).then((ev) => {
						let response = JSON.parse(ev.target.responseText);
						this.get('model').set(this.get('field'), response.url);
					});
					return false;
				});
			}
			$(this.element.querySelector('[data-role=upload-image-button]')).off('click');
			$(this.element.querySelector('[data-role=upload-image-button]')).on('click', () => {
				this.actions.preupload.bind(this)();
			});
			inputFile.onchange = () => {
				this.element.dispatchEvent(new CustomEvent('drop', {
					'detail': {
						files: inputFile.files
					}
				}));
        inputFile.value = null;
			};
		}

		if (this.configuration.catalog) {
			// this._targetObject.sideModels[this.configuration.catalog]().then(records => {
			//   Ember.set(this, 'records', records);
			// });
			$(this.element.querySelectorAll('select')).material_select('destroy');
			let uninitelement = $(this.element).find('select:not(.initialized)').get(0);
			// console.log(uninitelement);
			if (uninitelement) {
				// $(this.element).find('select:not(.initialized)').get(0).onchange = alert;
				$(this.element.querySelectorAll('select')).material_select(() => {
					this.set('selected', this.element.querySelector('select.initialized').value);
				// this.element.querySelector('input').value = this.element.querySelector('select.initialized').value;
				});
			}
		}
	},
	willDestroyElement: function willDestroyElement () {
		this._super.apply(this, arguments);
		$(this.element.querySelectorAll('select')).material_select('destroy');
	}
});
