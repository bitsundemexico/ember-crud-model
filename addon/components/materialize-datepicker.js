import Ember from 'ember';
import layout from '../templates/components/materialize-datepicker';

export default Ember.Component.extend({
  layout,
  tagName: 'input',
  classNames: 'datepicker',
  attributeBindings: ['value:data-value', 'name'],
  didInsertElement(){
    let that = this;
    $(this.element).pickadate({
      closeOnSelect: true,
      format: 'dddd, dd mmm, yyyy',
      onSet () {
        let value = (this.get('select') || {}).obj;
        that.set('value', value);
      },
      selectMonths: true,
      selectYears: 15
    });
    this.element.setAttribute('required', this.get('required'));
    this.element.removeAttribute('readonly');
    // if (this.configuration.type === 'date' && this.materialize) {
		// 	$(this.element.querySelector(this.get('id'))).val(this.get('model').get(this.field));
    //
		// }
  }
});
