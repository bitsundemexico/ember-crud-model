/*global $*/
import Ember from 'ember';
// import layout from '../templates/components/crud-table';

export default Ember.Component.extend({
  canupdate: true,
  candelete: true,
  hasactions: Ember.computed('canupdate', 'candelete', function(){
    return this.get('canupdate') || this.get('candelete');
  }),
  materialize: true,
  i18n: Ember.inject.service(),
  layoutName: 'crud-model/components/crud-table',
  tagName:'table',
  classNames:["table"],
  classHeaders:[],
  actions:{
    generic (action, ...args) {
      let fn = (this._targetObject.genericActions && this._targetObject.genericActions[action]) || (this._targetObject.actions && this._targetObject.actions[action]);
			if (fn) {
				fn.bind(this)(...args);
			}
		},
    toggleClass (selector, className, className2) {
      selector = $(selector);
      if(selector.hasClass(className)) {
        selector.removeClass(className);
        selector.addClass(className2);
      } else {
        selector.removeClass(className2);
        selector.addClass(className);
      }
    },
    edit(record){
      this.attrs.edit(record);
    },
    delete(record){
      this.attrs.delete(record);
    },
    toggleSort(sortable){
      this.attrs.toggleSort(sortable);
    }
  },
  didRender(){
    $('[data-toggle=tooltip]').tooltip('dispose');
    $('[data-toggle=tooltip]').tooltip();
  }
});
