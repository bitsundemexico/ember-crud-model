import Ember from 'ember';
import layout from '../templates/components/google-map';

export default Ember.Component.extend({
  layout,
  tagName: 'section',
  map: null,
  zoom: 12,
  classNames: ['google-map-canvas'],
  location () {
    let [lat, lng] = this.get('center');
    return {
      lat: parseFloat(lat, 10),
      lng: parseFloat(lng, 10)
    };
  },
  center: [20.677632, -101.3416320],
  enabled: true,
  initialized: false,
  updateMarker: Ember.computed('markerPrecision', 'initialized', 'enabled', function() {
    let marker;
    let enabled = this.get('enabled');
    let map = this.get('map');
    if(!map)return enabled ? '':'hide';
    if(this.get('marker')) {
      this.get('marker').setMap(null);
    }
    if (enabled && this.get('markerPrecision')) {
      setTimeout(()=>{
        map.setZoom(18);
        map.setCenter(this.location());
      }, 300);

      marker = new window.google.maps.Marker({
        map,
        position: this.location(),
        draggable: true,
        animation: window.google.maps.Animation.DROP,
        title: 'New State'
      });
    } else if(enabled) {
      map.setZoom(15);
      map.setCenter(this.location());
      marker = new google.maps.Circle({
        map,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        center: this.location(),
        radius: this.get('zoom') * 40
      });
    }
    this.set('marker', marker);
    return this.get('enabled') ? '':'hide';
  }),
  markerPrecision: true,
  actions: {
    findlocation () {
			let map = this.get('map');
      let zoom = this.get('zoom');
      if( zoom < 16) {
        zoom = 16;
      }
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition((position) => {
					let pos = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					this.get('marker').setPosition(pos);
					map.setCenter(pos);
					map.setZoom(zoom);
					this.set('zoom', zoom);
          this.set('center', [position.coords.latitude, position.coords.longitude]);
          this.mapDecodeLocation();
				}, function () {
					// handleLocationError(true, infoWindow, map.getCenter());
				});
			} else {
				// Browser doesn't support Geolocation
				// handleLocationError(false, infoWindow, map.getCenter());
			}
		}
  },
  didInsertElement() {
    this.set('initialized', false);
    this.initializeMap();
  },
  initializeMap() {
    if(!this.get('center')) {
      this.set('center', [20.677632, -101.3416320]);
    }
    if(!this.get('zoom')) {
      this.set('zoom', 12);
    }
    let mapit = (id, latlng) => {
      if (document.getElementById(id) === null) {
        return false;
      }
      let mapOptions = {
        zoom: this.get('zoom'),
        center: this.location(),
        mapTypeId: window.google.maps.MapTypeId.ROADMAP
      };
      let map = new window.google.maps.Map(document.getElementById(id), mapOptions);
      window.google.maps.event.addListener(map, 'zoom_changed', (event) => {
        this.set('zoom', map.get('zoom'));
      });
      window.google.maps.event.addListener(map, 'click', (event) => {
        this.set('center', [event.latLng.lat(), event.latLng.lng()]);
        if ( this.get('marker') ) {
          this.get('marker').setPosition(event.latLng);
        }
      });
      this.set('map', map);
      this.set('initialized', true);
    };
    if (!window.usegooglemaps) {
      return;
    }
    let waitforgoogle = (fn) => {
      if (window.mapstarted === undefined) {
        setTimeout(function() {
          fn(fn);
        }, 10);
        return false;
      }
      mapit('google_map_canvas');
    };
    setTimeout(function() {
      waitforgoogle(waitforgoogle);
    }, 1000);
  },
  mapDecodeLocation () {
		let _this = this;
		let geocoder = new window.google.maps.Geocoder();
		geocoder.geocode({
			'latLng': this.get('marker').getPosition()
		}, function (results, status) {
			if (status === window.google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					_this.set('guessedAdress', results[0].formatted_address);
				} else {
					_this.set('guessedAdress', 'address not found');
				}
			} else {
				_this.set('guessedAdress', 'Geocoder failed due to: ' + status);
			}
		});
	}
});
