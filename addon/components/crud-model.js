import Ember from 'ember';
// import paginator from 'crud-model/paginator';
// import filterset from 'crud-model/filterset';

const {set , RSVP} = Ember;
function capitalize (str) {
	return str[0].toUpperCase() + str.substring(1);
}
const fieldOptions = {
	id: null,
	list: true,
	create: true,
	update: true,
	delete: true,
	display: true,
	type: 'text',
	default: null,
	required: false,
	haswarnings: false,
	sort: true,
	search: false,
  checked: 'check',
  unchecked: 'minus',
	sufix: false
};

export default Ember.Component.extend({
  __isCrudModel: true,
  layoutName: 'crud-model/components/crud-model',
	materialize: true,
  tableTagName: 'table',
	tableClasses:['table'],
  tableTemplate: 'crud-model/table',
	labelSize: 2,
	inputSize: Ember.computed('labelSize', function () {
		return 12 - this.get('labelSize');
	}),
  canupdate: true,
  candelete: true,
  cancreate: true,
	showFields: true,
	tagName: 'section',
	classNames: ['crud-model'],
	fields: true,
	refresh: true,
	model: null,
	datamodel: null,
	fetch() {
		let deferred = this.wait('read', 'loading');
		this.paginator.query(this.query.body);
		this.filterset.query(this.query.body);
		this.read(this.query.body, deferred);
		deferred.promise.then(records => {
			this.paginator.extractMeta(records);
			set(this, 'datamodel', records);
		});
	},
	query: null,
	selectedItem: null,
	searchProperty: null,
  sortingProperty: null,
  sortDirection: 'sort',
  orderBy: false,
	searchTerm: null,
	mode: null,
	sideModels: null,
  genericActions: null,
  customActions: null,
	actions: {
    setSorting (field) {
      // this.set(`model.${field}.sort`, )
      this.set('sortingProperty', field);
      let sort = this.get('sortDirection');
      if (sort.indexOf('desc') > -1) {
        sort = 'sort-asc';
      } else if (sort.indexOf('asc') > -1) {
        sort = 'sort';
      } else {
        sort = 'sort-desc';
      }
      this.set('sortDirection', sort);
      this.set(`model.${this.get('sortingProperty')}.sort`, sort);
      clearTimeout(this.get('waitforsort'));
			this.set('waitforsort', setTimeout(() => {
				delete this.query.body.sort;
				if (sort !== 'sort') {
					this.paginator.sort(this.query.body, field, sort === 'sort-desc'? 1: 0);
				}
				this.fetch();
			}, 1300));
    },
		CloseFilterSet(result) {
			if (result) {
				if (this.attrs.savefilter !== undefined) {
					let filterset = this.filterset.filtergroups.content.map((filtergroup) => {
						return {
							link: filtergroup.Link,
							filters: filtergroup.content.map((filter) => {
								return {
									key: filter.Key,
									display: filter.Display,
									condition: filter.Condition,
									value: filter.Value,
									valuemask: filter.ValueMask,
									link: filter.Link
								};
							})
						};
					});
					result = !this.attrs.savefilter.bind(this.get('parent'))(filterset, this.filterset.filtergroups.get('areValid'));
				}
			}
			this.set('filtersetStatus', result);
		},
		AddFilter(filtergroup) {
			let count = filtergroup.get('length');
			if (filtergroup.get('length') > 0) {
				filtergroup.objectAt(filtergroup.get('length') - 1).set('Link', 'or');
			}
			filtergroup.addObject(this.filterset.create());
		},
		RemoveFilter(filtergroup, filter) {
			filtergroup.removeObject(filter);
			let prevfilter = filtergroup.objectAt(filtergroup.get('length') - 1);
			if (prevfilter) {
				prevfilter.set('Link', '');
			}
		},
		SwitchLocale(filter) {
			filter.set('Link', filter.Link === 'or' ? 'and' : 'or');
		},
		SelectFilterProperty(filter, property) {
			let selectobject = false;
			filter.set('Key', property);
			filter.set('Type', this.model[property].type);
			let type = this.model[property].type.split(':')[0];

			filter.set('Options', this.filterset[this.model[property].type]);
			filter.set('Display', this.model[property].label);
			switch (type) {
				case 'record':
					filter.set('Key', `${property}.${this.model[property].source}`);
					filter.set('Options', this.filterset.text);
					break;
				case 'ref.search':
					filter.set('Key', this.model[property].SearchValue);
					filter.set('Options', this.filterset.text);
					break;
				case 'number':
				case 'text':
					break;
				case 'many-multi':
				case 'belongsto':
				case 'powerselect': selectobject = {};
					this.dependants[this.model[property].display].forEach((element) => {
						selectobject[element.id] = element.get(this.model[property].display);
					});
					// filter.set('Key',this.model[property].Source + '.' + this.model[property].Display);
					break;
			}
			filter.set('SelectObject', selectobject);
		},
		SelectFilterValue(filter, target) {
			filter.set('Value', target.value);
			filter.set('ValueMask', target.selectedOptions[0].innerText);
		},
		SelectFilterConditon(filter, value) {
			filter.set('Condition', value);
		},
		filter_addGroup() {
			this.filterset.AddFilterGroup();
		},
		filterset() {
			this.filterset.init();
			this.set('filtersetStatus', true);
		},
		set(target, property, value) {
			Ember.set(target, property, value);
		},
		setLimit(limit) {
			this.paginator.set('limit', limit);
			this.fetch();
		},
		child() {
			return this;
		},
		goto(page) {
			if (page) {
				this.paginator.query(this.query.body, page);
				this.fetch();
			}
		},
		cancel() {
			let record = this.get('selectedItem');
			if (record.get('isNew')) {
				this.datamodel.removeObject(record);
				record.deleteRecord();
			}
			if (record.get('hasDirtyAttributes')) {
				record.rollbackAttributes();
			}
			this.set('selectedItem', null);
			this.set('mode', null);
		},
		edit(record) {
			if (this._targetObject.actions.edit){
				this._targetObject.actions.edit.bind(this._targetObject)(record);
			} else {
				this.set('selectedItem', record);
				this.set('mode', 'update');
			}
		},
		delete(record) {
			if (this._targetObject.actions.remove){
				this._targetObject.actions.remove.bind(this._targetObject)(record);
			} else {
				this.set('selectedItem', record);
				this.set('mode', 'delete');
			}
		},
		createRecord() {
			let wait = this.wait('newRecord');
			this._targetObject.actions.record.bind(this._targetObject)(wait);
			wait.promise.then((record) => {
				this.set('selectedItem', record);
				// this.datamodel.pushObject(record._internalModel);
				this.set('mode', 'create');
			});
		},
		refresh() {
			this.fetch();
		},
		toggleField(fieldconfig) {
			set(fieldconfig, 'display', !fieldconfig.display);
		},
		searchField(field) {
			this.set('searchProperty', field);
		},
		searchLocal() {
			let term = this.get('searchTerm');
			let property = this.get('searchProperty');
			let results = [];
			this.datamodel.forEach((record) => {
				if ((record.get(property) || '').indexOf(term) > -1) {
					results.push(record);
				}
			});
			this.set('original_model', this.get('datamodel'));
			this.set('datamodel', results);
		},
		clearSearch() {
			this.set('datamodel', this.get('original_model'));
			this.set('original_model', null);
			this.set('searchTerm', null);
		},
		toggleSort(field) {
			let sort = this.get(`model.${field}.sort`);
			Object.keys(this.model).forEach((key) => {
				let data = this.model[key];
				if (data.sort) {
					set(data, 'sort', 'sort');
				}
			});
			if (sort.indexOf('desc') > -1) {
				sort = 'sort-asc';
			} else if (sort.indexOf('asc') > -1) {
				sort = 'sort';
			} else {
				sort = 'sort-desc';
			}
			this.set(`model.${field}.sort`, sort);
			clearTimeout(this.get('waitforsort'));
			this.set('waitforsort', setTimeout(() => {
				delete this.query.body.sort;
				if (sort !== 'sort') {
					this.paginator.sort(this.query.body, field, sort === 'sort-desc'? 1: 0);
				}
				this.fetch();
			}, 1300));
		},
		custom(action, ...params) {
			this.findDefaultAction(action);
			this.attrs[action](...params);
		}
	},
	targetObjectActions() {
		return this._targetObject.actions;
	},
	findDefaultAction(action) {
		if (!this.attrs[action] && !!this.targetObjectActions()[action]) {
			this[action] = this.attrs[action] = this.targetObjectActions()[action].bind(this._targetObject);
		}
	},
	wait(id, toggle = '' ) {
		set(this, toggle, true);
		let deferred = RSVP.defer(`crud-model#${id}`);
		deferred.promise.catch(err => {
			set(this, toggle, false);
			return err;
		}).then((r) => {
			set(this, toggle, false);
			return r;
		});
		return deferred;
	},
	init() {
		this._super(...arguments);
    this.set('genericActions', {});
    this.set('customActions', {});
    this.set('sideModels', {});
		this.set('query', {body: {}});
    this.set('paginator', this.get('paginator')());
		this.set('filterset', this.get('filterset')());
		for (let field of Object.keys(this.model)) {
      Ember.set(this.model, field, Object.assign({}, fieldOptions, Ember.get(this.model, field)));
			if ( !Ember.get(this.model, 'id') ) {
				Ember.set(this.model, 'id', Ember.get(this.model,'field'));
			}
			this.model[field].label = this.model[field].label || capitalize(field);
			if(this.model[field].type.indexOf(':') > -1 ) {
				let data = this.model[field].type.split(':');
				this.model[field].type = data[0];
				this.model[field].template = data[1];
        this.model[field].list = false;
        this.customActions[field] = this.model[field];
			}
			if (this.model[field].type === 'record' && this.model[field].records) {
				let action = this.model[field].records;
				Ember.set(this.sideModels, action, this.targetObjectActions()[action].bind(this._targetObject));
			}
			if ( this.model[field].catalog ) {
				let action = this.model[field].catalog.records;
				Ember.set(this.sideModels, action, this.targetObjectActions()[action].bind(this._targetObject));
			}
      if (this.model[field].action) {
        Ember.set(this.customActions, field, this.model[field]);
        Ember.set(this.genericActions, this.model[field].action, this.targetObjectActions()[this.model[field].action].bind(this._targetObject));
      }
			if (this.model[field].search && !this.get('searchProperty')) {
				this.set('searchProperty', field);
			}
			if (this.model[field].sort) {
				this.model[field].sort = 'sort';
			}
		}
		['create', 'read', 'update', 'delete', 'errorParser' , 'savefilter', 'resolve'].forEach(action => this.findDefaultAction(action));
	},
	didReceiveAttrs() {
		this._super(...arguments);
		this.fetch();
	},
	didRender() {
		this._targetObject.set('ecm', this);
		// $('.dropdown-button').dropdown({
	  //   constrainWidth: false,
	  //   stopPropagation: true
	  // });
	}
});
