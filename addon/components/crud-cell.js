var templates = [];
  for (var moduleKey in require._eak_seen) {
    if (moduleKey.indexOf('\/templates\/') !== -1) {
      if (moduleKey.indexOf('crud-model/fields/') > -1) {
        templates.push(moduleKey);
      }
    }
  }

  export default Ember.Component.extend({
    tempvalue: null,
    selected: null,
    configuration: null,
    materialize: true,
    i18n: Ember.inject.service(),
    actions: {},
    mode: 'read',
    layoutName: Ember.computed('configuration', function () {
      var template = 'read-' + (this.get('configuration.template') || this.get('configuration.type'));
      template = templates.any(function (t) {
        return t.indexOf(template) > -1;
      }) ? template : 'read-text';
      return 'crud-model/fields/' + template;
    }),
    init () {
      this._super(...arguments);
      if (this.configuration.action) {
        let crud_model = this;
        while( !crud_model.get('__isCrudModel') && !crud_model.get('isController'))
        {
          crud_model = crud_model._targetObject;
        }
        if ( !crud_model.get('isController') ) {
          let action = crud_model.get('genericActions')[this.configuration.action];
          this.actions[this.configuration.action] = action.bind(this);
        }
      }
    },
    generic () {
			if (this.configuration.action) {
				// for (let _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
				// 	args[_key] = arguments[_key];
				// }

				if (this._targetObject.genericActions) {
					// let _targetObject$generic;
          this._targetObject.genericActions[this.configuration.action](...arguments, this, this._targetObject);

					// (_targetObject$generic = this._targetObject.genericActions)[this.configuration.action].apply(_targetObject$generic, _toConsumableArray(args).concat([this, this._targetObject]));
				} else {
					// let _targetObject$actions;
          this._targetObject.actions[this.configuration.action](...arguments, this, this._targetObject);
					// (_targetObject$actions = this._targetObject.actions)[this.configuration.action].apply(_targetObject$actions, _toConsumableArray(args).concat([this, this._targetObject]));
				}
			}
		},
    timeout: 0,
    tagName: 'td',
    classNameBindings: []
  });
