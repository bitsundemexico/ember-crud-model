/*global $*/
import Ember from 'ember';
import validateform from '../utils/validate-form';
const {
  computed
} = Ember;

export default Ember.Component.extend({
  layoutName: 'crud-model/components/crud-modal',
  mode: 'read',
  sideModels: {},
  mode_create: computed('mode', function() {
    return this.mode === 'create';
  }),
  mode_update: computed('mode', function() {
    return this.mode === 'update';
  }),
  mode_delete: computed('mode', function() {
    return this.mode === 'delete';
  }),
  actions: {
    set(target, property, value) {
      Ember.set(target, property, value);
    },
    // cancel() {
    // 	$('#crud-model-modal').modal('close');
    // },
    validate() {
      let deferred = null;
      switch (this.get('mode')) {
        case 'create':
        case 'update':
          $('#crud-model-alert').collapsible('close');
          if (validateform(this.get('fields'), this.element.querySelector('form'))) {
            deferred = this.attrs.parent().wait(this.get('mode'), 'loading');
            this.get(this.get('mode'))(this.get('model'), deferred);
            this.set('wait', true);
          } else {
            this.element.querySelector('[type=submit]').click();
          }
          break;
        case 'delete':
          deferred = this.attrs.parent().wait(this.get('mode'), 'loading');
          this.get(this.get('mode'))(this.get('model'), deferred);
          break;
      }
      if (deferred) {
        this.get('resolve')(deferred.promise, $('#crud-model-alert'), this.get('errorParser')).then(res => {
          this.set('wait', false);
          if(res) {
            $('#crud-model-modal').modal('close');
          }
          this.attrs.parent().fetch();
        });
        // if (this.get('resolve')) {
        //
        //   });
        // } else {
        //   deferred.promise.catch((err) => {
        //     let message = this.get('errorParser')(err);
        //     $('#crud-model-alert').html(message);
        //     $('#crud-model-alert').collapsible('open');
        //     return 'error';
        //   }).then((res) => {
        //     if (res !== 'error') {
        //       $('#crud-model-modal').modal('close');
        //     }
        //     this.attrs.parent().fetch();
        //   });
        // }
      }
    }
  },
  materialize: false,
  classNameBindings: ['materialize:mt-form:'],
  init() {
    this._super(arguments);
  },
  didInsertElement() {
    let component = this;
    // $(this.element).find('#crud-model-modal').off('hide.bs.modal');
    // $(this.element).find('#crud-model-modal').off('hidden.bs.modal');
    // $(this.element).find('#crud-model-modal').off('show.bs.modal');
    // $(this.element).find('#crud-model-modal').on('hide.bs.modal', (ev) => {
    //   if (ev.target.id === "crud-model-modal") {
    //     this.attrs.cancel();
    //   }
    // });
    // $(this.element).find('#crud-model-modal').on('hidden.bs.modal', (ev) => {
    //   if (ev.target.id === "crud-model-modal") {
    //     $('#crud-model-alert').collapsible('close');
    //   }
    // });
    // $(this.element).find('#crud-model-modal').on('show.bs.modal', (ev) => {
    //   // if (ev.target === this) {
    //     this.set('parent', this.attrs.parent());
    //     this.set('mode', this.attrs.parent().get('mode'));
    //     this.set('model', this.attrs.parent().get('selectedItem'));
    //   // }
    // });
    component.set('parent', component.attrs.parent());
    component.set('mode', component.attrs.parent().get('mode'));
    component.set('model', component.attrs.parent().get('selectedItem'));
    $(this.element).find('#crud-model-modal').modal({
      complete() {
        component.attrs.cancel();
      },
      ready(){
      }
    });
    setTimeout(()=>{
      $(this.element).find('#crud-model-modal').modal('open');
    }, 100);

  }
});
