/* eslint-env node */
'use strict';
const DEFAULT_OPTIONS = {
	importFontAwesome: true,
	importIonIcons: true
};
module.exports = {
	name: 'ember-crud-model',
	options: {
		nodeAssets: {
      'materialize-css': {
        import:{
          srcDir: 'dist',
          include: [
            'js/materialize.js'
          ]
        }
      },
			'themify-icons': function () {
				return {
					public: {
						srcDir: 'themify-icons',
						include: [ 'fonts/*' ]
					}
				};
			},
      'cropperjs': {
        import:{
          srcDir: 'dist',
          include: ['cropper.min.css','cropper.min.js']
        }
      },
			'ionicons': function () {
				return {
					public: {
						srcDir: 'dist',
						destDir: 'assets/ionicons/',
						include: [...this.ionIcons]
					}
				};
			},
			'font-awesome': function () {
				return {
					public: {
						destDir: '/',
						include: [this.fontAwesome[0]]
					},
          import: [this.fontAwesome[1]]
				};
			}
		}
	},
	included: function (app) {
    // let fontAwesomeIcons = fs.readFileSync('node_modules/font-awesome/scss/_variables.scss').match(/\$fa-var-(.*):.*;[\r|\n]?/m);
		const options = Object.assign({}, DEFAULT_OPTIONS, app.options['onsen-ui']);
		this.fontAwesome = options.importFontAwesome ? ['fonts/*', 'css/font-awesome.min.css'] : [null,null];
		this.ionIcons = options.importIonIcons ? ['fonts/*', 'css/ionicons.min.css'] : [null, null];
		this._super.included.apply(this, arguments);
    app.import('vendor/ember-crud-model.css');
	}
};
