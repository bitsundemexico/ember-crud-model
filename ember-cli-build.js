/* eslint-env node */
const EmberAddon = require('ember-cli/lib/broccoli/ember-addon');
// const nodeSass = require('node-sass');

module.exports = function(defaults) {
  var app = new EmberAddon(defaults, {
  });

  return app.toTree();
};
