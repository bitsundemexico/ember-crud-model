import Ember from 'ember';
export default Ember.Helper.helper(([target, property, value]) => {
  return ()=>{
    Ember.set(target, property, value);
  };
});
