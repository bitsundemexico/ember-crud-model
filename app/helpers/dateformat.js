import Ember from 'ember';
let picker = $(`<input>`).pickadate();
let picka = picker.pickadate('picker');

export default Ember.Helper.helper(([date, format]) => {
  picka.set('select', date, {format});
  let value = picker.get(0).value;
  return value;
});
