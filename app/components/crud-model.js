import Ember from 'ember';
import crudmodel from 'ember-crud-model/components/crud-model';
import paginator from '../crud-model/paginator';
import filterset from '../crud-model/filterset';


// crudmodel.set('paginator', () => {
//   return paginator.create();
// });
// crudmodel.set('filterset', () => {
//   return filterset.create();
// });

export default crudmodel.extend({
  paginator () {
    return paginator.create();
  },
  filterset () {
    return filterset.create();
  }
});
