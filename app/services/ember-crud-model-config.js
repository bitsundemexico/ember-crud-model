import Ember from 'ember';
import ENV from '../config/environment';
export default Ember.Service.extend({}, {
  ['password-policy']: {
    restrict_chars: [',','"',"'",'`','^'],
    numbers: 2,
    mayus: 2,
    length: 12,
    specialChars: 2,
    blacklist: ['1234567890']
  },
  'google' : {
    key: null
  }

}, ENV['ember-crud-model'] || {});
