import Ember from 'ember';
export default {
  name: 'i18n',
  initialize: function(appInstance) {
    let i18n = appInstance.lookup('service:i18n');
    i18n.set('locale', calculateLocale(i18n.get('locales')));
  }
}

function calculateLocale(locales) {
  // whatever you do to pick a locale for the user:
  const language = navigator.language || navigator.userLanguage;


  Ember.$.extend(Ember.$.fn.pickadate.defaults, {
    monthsFull: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
    monthsShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
    weekdaysFull: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
    weekdaysShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
    today: 'hoy',
    clear: 'borrar',
    close: 'cerrar',
    firstDay: 1,
    format: 'dddd d !de mmmm !de yyyy',
    formatSubmit: 'yyyy/mm/dd',
    labelMonthNext: "Me Siguiente",
    labelMonthPrev: "Mes Anterior",
    labelMonthSelect: "Selecciona un Mes",
    labelYearSelect: "Selecciona un Año"
  });
  return locales.includes(language.toLowerCase()) ? language : 'en';
}
