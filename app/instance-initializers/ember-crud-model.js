export default {
	name: 'google-api-for-crud-model',
	initialize: function initialize (instance) {
		let config = instance.lookup('service:ember-crud-model-config');
		if (config.get('google').key) {
			window.usegooglemaps = true;
			window.onload = function () {
				window.initMap = function () {
					window.mapstarted = true;
				};
				let script = document.createElement('script');
				script.type = 'text/javascript';
				script.src = window.location.protocol + '//maps.googleapis.com/maps/api/js?key=' + config.google.key + '&callback=initMap';
				document.body.appendChild(script);
			};
		}
	}
};
