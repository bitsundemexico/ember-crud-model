import Ember from 'ember';
import paginator from 'ember-crud-model/mixins/paginator/base';
export default Ember.Object.extend(paginator);
