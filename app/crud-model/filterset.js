import Ember from 'ember';
import filterset from 'ember-crud-model/mixins/filterset/base';
export default Ember.Object.extend(filterset);
